# Copyright (c) 2015 M. Rizky Luthfianto
# This code is protected under Creative Commons License:
# http://creativecommons.org/licenses/by-nc-nd/4.0/

# References:
# - Argawal, S., 'CASH DEMAND FORECASTING FOR ATMS', Master Thesis, Indian Institute of Technology Kanpur, 2013.
#   Available from http://www.idrbt.ac.in/PDFs/PT%20Reports/2013/Shanu%20Agrawal_Cash%20demand%20forecasting%20for%20ATMs_2013.pdf
# - Ekinci, Y. et al., 'Optimization of ATM cash replenishment with group-demand forecasts', Expert Systems with Applications, vol. 42, no. 7, 2015, pp. 3480-3490.
#   Available from http://www.researchgate.net/publication/270969041_Optimization_of_ATM_cash_replenishment_with_group-demand_forecasts

library(forecast)
library(Matrix)
df = read.csv("B:\\r\\BRANCH DATA HISTORY.txt", header = TRUE, sep = '|')
branch132df=df[df$BRCODE==132,]
branch132 <- ts(branch132df$CLOSINGBAL, frequency=365)
#branch132 <- ts(branch132df$CLOSINGBAL)
plot.ts(branch132)
#branch132.arima <- arima( branch132, order=c(1,1,0), xreg = c('flag500'))

flag500 <- function(deposit){
  if (deposit<500000){
    return(1)
  } else {
    return(0)
  }
}

for (i in 1:length(branch132df$DEPOSIT)) {
  if (branch132df$DEPOSIT[i]<=250000){
    branch132df$flag250[i]=1
  } else {
    branch132df$flag250[i]=0
  }
  
  if (branch132df$DEPOSIT[i]>250000 & branch132df$DEPOSIT[i]<=500000){
    branch132df$flag500[i]=1
  } else {
    branch132df$flag500[i]=0
  }
  
  if (branch132df$DEPOSIT[i]>500000 & branch132df$DEPOSIT[i]<=750000){
    branch132df$flag750[i]=1
  } else {
    branch132df$flag750[i]=0
  }
  
  if (branch132df$DEPOSIT[i]>750000 & branch132df$DEPOSIT[i]<=1000000){
    branch132df$flag1000[i]=1
  } else {
    branch132df$flag1000[i]=0
  }
  
  if (branch132df$DEPOSIT[i]>1000000 & branch132df$DEPOSIT[i]<=1250000){
    branch132df$flag1250[i]=1
  } else {
    branch132df$flag1250[i]=0
  }
  
  if (branch132df$DEPOSIT[i]>1250000 & branch132df$DEPOSIT[i]<=1500000){
    branch132df$flag1500[i]=1
  } else {
    branch132df$flag1500[i]=0
  }
  
  if (branch132df$DEPOSIT[i]>1500000 & branch132df$DEPOSIT[i]<=1750000){
    branch132df$flag1750[i]=1
  } else {
    branch132df$flag1750[i]=0
  }
  if (branch132df$DEPOSIT[i]>1750000 & branch132df$DEPOSIT[i]<=2000000){
    branch132df$flag2000[i]=1
  } else {
    branch132df$flag2000[i]=0
  }
  if (branch132df$DEPOSIT[i]>2000000 & branch132df$DEPOSIT[i]<=2250000){
    branch132df$flag2250[i]=1
  } else {
    branch132df$flag2250[i]=0
  }
  if (branch132df$DEPOSIT[i]>2250000 & branch132df$DEPOSIT[i]<=2500000){
    branch132df$flag2500[i]=1
  } else {
    branch132df$flag2500[i]=0
  }
}

#xreg<-cbind(branch132df$flag200,branch132df$flag400,branch132df$flag600,branch132df$flag800,branch132df$flag1000,branch132df$flag1200,branch132df$flag1400)
#xreg<-as.matrix(cbind(branch132df$flag250,branch132df$flag500,branch132df$flag750,branch132df$flag1000,branch132df$flag1250,branch132df$flag1500,branch132df$flag1750,branch132df$flag2000,branch132df$flag2250,branch132df$flag2500))
#xreg<-as.matrix(cbind(branch132df$flag250,branch132df$flag1000,branch132df$flag1250,branch132df$flag1500,branch132df$flag1750,branch132df$flag2250))

xreg<-as.matrix(cbind(branch132df$flag250,branch132df$flag1000,branch132df$flag1250,branch132df$flag1500,branch132df$flag1750,branch132df$flag2250,branch132df$flag2500))

#xreg_w <- seasonaldummy(ts(branch132,f=7))
branch132.arima <- auto.arima( branch132, xreg = xreg)
branch132.arimaf <- forecast.Arima(branch132.arima,xreg = xreg)
#summary( branch132.arimaf )
forecast.Arima('CLOSINGBAL~ DEPOSIT', branch132)
#plot.forecast( branch132.arimaf)
summary( branch132.arimaf)
plot.forecast( branch132.arimaf)